require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "page_title" do
    let(:base_title) { ApplicationHelper::BASE_TITLE }

    context "can display fulltitle when visit pages" do
      it "display 'page_title - PotePanApp'" do
        expect(full_title("page_title")).to eq("page_title - #{ApplicationHelper::BASE_TITLE}")
      end
    end

    context "cannot display fulltitle when visit pages" do
      it "display only 'BASE_TITLE' when pagetitle nil" do
        expect(full_title(nil)).to eq ApplicationHelper::BASE_TITLE
      end

      it "display only 'BASE_TITLE' when pagetitle blank" do
        expect(full_title("")).to eq ApplicationHelper::BASE_TITLE
      end
    end
  end
end
