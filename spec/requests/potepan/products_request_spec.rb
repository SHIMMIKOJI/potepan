require "rails_helper"

RSpec.describe "display product-page", type: :request do
  describe "GET/SHOW" do
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "returns http success" do
      expect(response.status).to eq 200
    end

    it "return product name" do
      expect(response.body).to include product.name
    end

    it "return product description" do
      expect(response.body).to include product.description
    end

    it "return product price" do
      expect(response.body).to include product.display_price.to_s
    end

    it "return product images" do
      product.images.each do |_image|
        expect(response.body).to include product.image.attachment(:large)
        expect(response.body).to include product.image.attachment(:small)
      end
    end

    it "have correct @related_product in 4" do
      expect(assigns(:related_products).size).to eq 4
    end
  end
end
