require "rails_helper"

RSpec.describe "display category-page", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:products) { create(:product, taxons: [taxon]) }

  describe "get categories/show" do
    before do
      get potepan_category_path(taxon.id)
    end

    it "returns http success" do
      expect(response.status).to eq 200
    end

    it "renders the categories/show" do
      expect(response).to render_template :show
    end

    it "taxon have expect-object" do
      expect(assigns(:taxon)).to match(taxon)
    end

    it "product heve expect-object" do
      expect(assigns(:products)).to match_array(products)
    end
  end
end
