require "rails_helper"

RSpec.describe "Product", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:unrelated_product) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "display related_product name & price" do
    within("div.productsContent") do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).not_to have_content product.name
    end
  end

  scenario "when clink related_product-name display product-detail-page" do
    click_on related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
